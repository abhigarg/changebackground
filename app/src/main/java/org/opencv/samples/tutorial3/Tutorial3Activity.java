package org.opencv.samples.tutorial3;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDouble;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import android.app.AlertDialog;
import android.content.Context;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

public class Tutorial3Activity extends Activity implements CvCameraViewListener2, OnTouchListener {
    private static final String TAG = "OCVSample::Activity";

    private Tutorial3View mOpenCvCameraView;
    private List<Size> mResolutionList;
    private MenuItem[] mEffectMenuItems;
    private SubMenu mColorEffectsMenu;
    private MenuItem[] mResolutionMenuItems;
    private SubMenu mResolutionMenu;

    private MenuItem[] mChooseCameraMenuItems;
    private SubMenu mChooseCameraMenu;
    private MenuItem[] mThresholdMenuItems;
    private SubMenu mThresholdMenu;

    private MenuItem[] mInitBackgroundMenuItems;
    private SubMenu mInitBackgroundMenu;

    private int gcount, threshVal, screen_h, screen_w;
    private String storagePath, initialBackgroundPath, newBackgroundImagePath, settingsTextFileName; // = Environment.getExternalStorageDirectory().getPath();
    private boolean setInitBack;

    // Mat containers for images
    //private int                    mViewMode;
    private Mat                    mRgba;
    private Mat                    mRgb_original;
    //private Mat                    mBgr_original;
    //private Mat                    mEqHistImg;
    private Mat                    mIntermediateMat;
    private Mat                    mDiffGray;
    private Mat                    mHSV;
    private Mat                    mLab;
    private Mat                    mImg;
    private Mat                    mImgTemp;
    private Mat                    mInitBack;
    private Mat                    mRealBack;
    private Mat                    mDiff;

    boolean isPictureTaken;
    private Scalar thresh1, thresh2;
    private List<Mat> mv_back_hsv;
    //private Mat img_h, back_h, hDiff;
    private Core.MinMaxLocResult resH, resS, resV;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(Tutorial3Activity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public Tutorial3Activity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.tutorial3_surface_view);

        mOpenCvCameraView = (Tutorial3View) findViewById(R.id.tutorial3_activity_java_surface_view);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

        mOpenCvCameraView.setCvCameraViewListener(this);

        mOpenCvCameraView.setCameraIndex(1);
        storagePath = Environment.getExternalStorageDirectory().getPath();
        initialBackgroundPath = storagePath + "/inital_background.jpg";
        newBackgroundImagePath = storagePath + "/changed_background";

        settingsTextFileName = "ChangeBackgroundSettings.txt";

        isPictureTaken = false;
        //File textFile = new File(storagePath + "/" + settingsTextFileName);


    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        setInitBack = false;
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mIntermediateMat = new Mat(height, width, CvType.CV_8UC1);
        mDiffGray = new Mat(height, width, CvType.CV_8UC1);
        //img_h = new Mat(height, width, CvType.CV_8UC1);
        //hDiff = new Mat(height, width, CvType.CV_8UC1);
        //back_h = new Mat(height, width, CvType.CV_8UC1);
        mHSV = new Mat(height, width, CvType.CV_8UC3);
        mLab = new Mat(height, width, CvType.CV_8UC3);
        mRgb_original = new Mat(height, width, CvType.CV_8UC3);
        //mBgr_original = new Mat(height, width, CvType.CV_8UC3);
        //mEqHistImg = new Mat(height, width, CvType.CV_8UC3);
        mImgTemp = new Mat(height, width, CvType.CV_8UC3);
        mDiff = new Mat(height, width, CvType.CV_8UC3);
        //mv_img_hsv = new ArrayList<Mat>(3);;
        mv_back_hsv = new ArrayList<Mat>(3);

        // Replace with User defined background
        mImg = new Mat(height, width, CvType.CV_8UC3, new Scalar(255, 255, 255));

        mRealBack = new Mat(height, width, CvType.CV_8UC3);

        mInitBack = loadHSVImageFromFile("inital_background.jpg", height, width); //RGB
        //Core.split(mInitBack, mv_back_hsv);
        //back_h = mv_back_hsv.get(0);

        Mat imgTemp = loadRGBImageFromFile("Tulips.jpg", height, width);  //RGB

        Imgproc.resize(imgTemp, mImgTemp, mImgTemp.size()); //RGB

        gcount = 0;
        threshVal = 20;

    }

    public void onCameraViewStopped() {

        mRgba.release();
        mDiffGray.release();
        mIntermediateMat.release();
        mHSV.release();
        mLab.release();
        mImg.release();
        mImgTemp.release();
        mInitBack.release();
        mRealBack.release();
        mDiff.release();

    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        gcount++;

        /*
        if(gcount==100) {
            System.gc();
            gcount = 0;
        }
*/
        mRgba = inputFrame.rgba();

        Core.flip(mRgba, mRgba, 1);

        mImgTemp.copyTo(mImg);  //RGB


        Imgproc.cvtColor(mRgba, mRgb_original, Imgproc.COLOR_RGBA2RGB);

        if(!setInitBack) {
/*
//rgb frame difference
            Core.absdiff(mRgb_original, mRealBack, mDiff);
            Imgproc.cvtColor(mDiff, mDiffGray, Imgproc.COLOR_RGB2GRAY);
            Imgproc.threshold(mDiffGray, mIntermediateMat, threshVal, 255, Imgproc.THRESH_BINARY);
            */

            // HSV frame differencing method
            Imgproc.cvtColor(mRgb_original, mHSV, Imgproc.COLOR_RGB2Lab);
            Core.absdiff(mHSV, mInitBack, mDiff);
            //Imgproc.cvtColor(mLab, mDiff, Imgproc.COLOR_Lab2BGR);
            Imgproc.cvtColor(mDiff, mDiffGray, Imgproc.COLOR_BGR2GRAY);
            Imgproc.threshold(mDiffGray, mIntermediateMat, threshVal, 255, Imgproc.THRESH_BINARY);
            //Core.inRange(mHSV, thresh1, thresh2, mIntermediateMat);

/*
//HSV ranges
            Imgproc.cvtColor(mRgb_original, mHSV, Imgproc.COLOR_RGB2HSV);

            Core.inRange(mHSV, thresh1, thresh2, mIntermediateMat);
            Imgproc.threshold(mIntermediateMat, mIntermediateMat, 0, 255, Imgproc.THRESH_BINARY_INV);
*/
            mRgb_original.copyTo(mImg, mIntermediateMat);

            Imgproc.cvtColor(mImg, mRgba, Imgproc.COLOR_RGB2RGBA);
        }

        return mRgba;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        List<String> effects = mOpenCvCameraView.getEffectList();

        if (effects == null) {
            Log.e(TAG, "Color effects are not supported by device!");
            return true;
        }

        mColorEffectsMenu = menu.addSubMenu("Color Effect");
        mEffectMenuItems = new MenuItem[effects.size()];

        int idx = 0;
        ListIterator<String> effectItr = effects.listIterator();
        while(effectItr.hasNext()) {
           String element = effectItr.next();
           mEffectMenuItems[idx] = mColorEffectsMenu.add(1, idx, Menu.NONE, element);
           idx++;
        }

        mInitBackgroundMenu = menu.addSubMenu("Set Background");
        mInitBackgroundMenuItems = new MenuItem[1];
        mInitBackgroundMenuItems[0] = mInitBackgroundMenu.add(3, 0, Menu.NONE, "Initialize Background");

        mThresholdMenu = menu.addSubMenu("Set Threshold values");
        mThresholdMenuItems = new MenuItem[3];
        mThresholdMenuItems[0] = mThresholdMenu.add(4, 0, Menu.NONE, "H value");
        mThresholdMenuItems[1] = mThresholdMenu.add(4, 1, Menu.NONE, "V value");
        mThresholdMenuItems[2] = mThresholdMenu.add(4, 2, Menu.NONE, "Diff Thresh");

        mResolutionMenu = menu.addSubMenu("Resolution");
        mResolutionList = mOpenCvCameraView.getResolutionList();
        mResolutionMenuItems = new MenuItem[mResolutionList.size()];

        ListIterator<Size> resolutionItr = mResolutionList.listIterator();
        idx = 0;
        while(resolutionItr.hasNext()) {
            Size element = resolutionItr.next();
            mResolutionMenuItems[idx] = mResolutionMenu.add(2, idx, Menu.NONE,
                    Integer.valueOf(element.width).toString() + "x" + Integer.valueOf(element.height).toString());
            idx++;
         }

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        if (item.getGroupId() == 1)
        {
            mOpenCvCameraView.setEffect((String) item.getTitle());
            Toast.makeText(this, mOpenCvCameraView.getEffect(), Toast.LENGTH_SHORT).show();
        }
        else if (item.getGroupId() == 2)
        {
            int id = item.getItemId();
            Size resolution = mResolutionList.get(id);
            mOpenCvCameraView.setResolution(resolution);
            resolution = mOpenCvCameraView.getResolution();
            String caption = Integer.valueOf(resolution.width).toString() + "x" + Integer.valueOf(resolution.height).toString();
            Toast.makeText(this, caption, Toast.LENGTH_SHORT).show();
        }
        else if(item.getGroupId() == 3)
        {
            setInitBack = true;
        }
        else if(item.getGroupId() == 4)
        {
            int id = item.getItemId();

            if(id == 0)
                minMaxHInputDialog();
            else if(id == 1)
                minMaxVInputDialog();
            else if(id == 2)
                setDiffThreshInputDialog();

        }

        return true;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.i(TAG,"onTouch event");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateandTime = sdf.format(new Date());
        String fileName = storagePath + "/sample_picture_" + currentDateandTime + ".jpg";

        if(setInitBack) {
            Mat im = new Mat();
            Imgproc.cvtColor(mRgb_original, im, Imgproc.COLOR_RGB2BGR);
            Imgcodecs.imwrite(initialBackgroundPath, im);

            Toast.makeText(this, "initial background set", Toast.LENGTH_SHORT).show();
            setInitBack = false;
            mInitBack = loadHSVImageFromFile("inital_background.jpg", mInitBack.height(), mInitBack.width()); //RGB

        }

        if(!setInitBack){
            mOpenCvCameraView.pauseCamera();
            Mat im = new Mat();
            Imgproc.cvtColor(mImg, im, Imgproc.COLOR_RGB2BGR);

            isPictureTaken = Imgcodecs.imwrite(newBackgroundImagePath + currentDateandTime + ".jpg", im);

            if(isPictureTaken) {
                Toast.makeText(this, "changed background image saved", Toast.LENGTH_SHORT).show();
                mOpenCvCameraView.startCamera();
            }
        }
        return false;
    }

    //load RGB image from sd card
    public Mat loadRGBImageFromFile(String fileName, int h, int w) {

        Mat rgbLoadedImage = null;

        String pathname = storagePath.toString();
        File file = new File(pathname, fileName);

        if(!file.exists()) {
            Toast.makeText(this, "Background image not found. Using default white background", Toast.LENGTH_SHORT).show();
            return new Mat(h, w, CvType.CV_8UC3, new Scalar(255, 255, 255));
        }
        else
            Toast.makeText(this, "Background loaded", Toast.LENGTH_SHORT).show();
        // this should be in BGR format according to the
        // documentation.

        Mat image = Imgcodecs.imread(file.getAbsolutePath());

        if (image.width() > 0) {

            rgbLoadedImage = new Mat(image.size(), image.type());

            Imgproc.cvtColor(image, rgbLoadedImage, Imgproc.COLOR_BGR2RGB);


                Log.d(TAG, "loadedImage: " + "channels: " + image.channels()
                        + ", (" + image.width() + ", " + image.height() + ")");

            image.release();
            //image = null;
        }

        return rgbLoadedImage;

    }

    //load HSV image from sd card
    public Mat loadHSVImageFromFile(String fileName, int h, int w) {

        Mat hsvLoadedImage = null;
        Mat image = null;

        String pathname = storagePath.toString();
        File file = new File(pathname, fileName);

        if(!file.exists()) {
            Toast.makeText(this, "Background image not found. Using default white background", Toast.LENGTH_SHORT).show();
            image = new Mat(h, w, CvType.CV_8UC3, new Scalar(255, 255, 255));

        }
        else {
            Toast.makeText(this, "Background loaded", Toast.LENGTH_SHORT).show();
            // this should be in BGR format according to the
            // documentation.

            image = Imgcodecs.imread(file.getAbsolutePath());
        }

        Imgproc.cvtColor(image, mRealBack, Imgproc.COLOR_BGR2RGB);  //RGB
        //Imgproc.equalizeHist(image, image);

        if (image.width() > 0) {

            hsvLoadedImage = new Mat(image.size(), image.type());

            Imgproc.cvtColor(image, hsvLoadedImage, Imgproc.COLOR_BGR2Lab);
            Core.split(hsvLoadedImage, mv_back_hsv);

            resH = Core.minMaxLoc(mv_back_hsv.get(0));
            resS = Core.minMaxLoc(mv_back_hsv.get(1));
            resV = Core.minMaxLoc(mv_back_hsv.get(2));

            thresh1 = new Scalar(resH.minVal, 0, 0);
            thresh2 = new Scalar(resH.maxVal, 255, 255);

            Write2TextFile(thresh1, thresh2);

            Toast.makeText(this, "Hue min max for new background: " + resH.minVal + ", " + resH.maxVal, Toast.LENGTH_SHORT).show();

            Log.d(TAG, "loadedImage: " + "channels: " + image.channels()
                    + ", (" + image.width() + ", " + image.height() + ")");

            Log.i(TAG, "Hue min max values: " + resH.minVal + ", " + resH.maxVal);
            Log.i(TAG, "Saturation min max values: " + resS.minVal + ", " + resS.maxVal);
            Log.i(TAG, "Values min max values: " + resV.minVal + ", " + resV.maxVal);

            image.release();
            //image = null;
        }

        return hsvLoadedImage;

    }


    private static Mat readInputStreamIntoMat(InputStream inputStream) throws IOException {
        // Read into byte-array
        byte[] temporaryImageInMemory = readStream(inputStream);

        // Decode into mat. Use any IMREAD_ option that describes your image appropriately
        Mat outputImage = Imgcodecs.imdecode(new MatOfByte(temporaryImageInMemory), Imgcodecs.IMREAD_GRAYSCALE);

        return outputImage;
    }

    private static byte[] readStream(InputStream stream) throws IOException {
        // Copy content of the image to byte-array
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[16384];

        while ((nRead = stream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();
        byte[] temporaryImageInMemory = buffer.toByteArray();
        buffer.close();
        stream.close();
        return temporaryImageInMemory;
    }


    protected void minMaxHInputDialog() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(Tutorial3Activity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog_cam_exposure, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Tutorial3Activity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(Tutorial3Activity.this, "Enter min max H value: " + editText.getText(),
                                Toast.LENGTH_SHORT).show();
                        String[] separated = editText.getText().toString().split(",");
                        thresh1.val[0] = Double.parseDouble(separated[0]);
                        thresh2.val[0] = Double.parseDouble(separated[1]);
                        Write2TextFile(thresh1, thresh2);
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    protected void minMaxVInputDialog() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(Tutorial3Activity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog_cam_exposure, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Tutorial3Activity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(Tutorial3Activity.this, "Enter min max V value: " + editText.getText(),
                                Toast.LENGTH_SHORT).show();
                        String[] separated = editText.getText().toString().split(",");
                        thresh1.val[2] = Double.parseDouble(separated[0]);
                        thresh2.val[2] = Double.parseDouble(separated[1]);
                        Write2TextFile(thresh1, thresh2);

                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    protected void setDiffThreshInputDialog() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(Tutorial3Activity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog_cam_exposure, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Tutorial3Activity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(Tutorial3Activity.this, "Frame diff thresh value: " + editText.getText(),
                                Toast.LENGTH_SHORT).show();
                        threshVal = Integer.parseInt(editText.getText().toString());

                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    // write text to file
    public void Write2TextFile(Scalar thresh1, Scalar thresh2) {

        String thresh1str = thresh1.val[0] + "," + thresh1.val[1] + "," + thresh1.val[2];

        String thresh2str = thresh2.val[0] + "," + thresh2.val[1] + "," + thresh2.val[2];

        File textFile = new File(storagePath + "/" + settingsTextFileName);

        if(!textFile.exists()){
            Toast.makeText(this, "Settings file does not exist. Creating new file", Toast.LENGTH_SHORT).show();

            try {
                textFile.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        // add-write text into file
        try {
            //OutputStreamWriter fileout= new openFileOutput(textFile.getName(), MODE_PRIVATE);
            OutputStreamWriter outputWriter=new OutputStreamWriter(openFileOutput(textFile.getName(), MODE_PRIVATE));
            outputWriter.write(thresh1str);
            //outputWriter.append(thresh2str);
            outputWriter.close();

            //display file saved message
            Toast.makeText(getBaseContext(), "File saved successfully!",
                    Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
/*
    // Read text from file
    public void ReadTextFile(View v) {
        //reading text from file
        try {
            FileInputStream fileIn=openFileInput("mytextfile.txt");
            InputStreamReader InputRead= new InputStreamReader(fileIn);

            char[] inputBuffer= new char[READ_BLOCK_SIZE];
            String s="";
            int charRead;

            while ((charRead=InputRead.read(inputBuffer))>0) {
                // char to string conversion
                String readstring=String.copyValueOf(inputBuffer,0,charRead);
                s +=readstring;
            }
            InputRead.close();
            Toast.makeText(getBaseContext(), s,Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/
}
